---
title: "Ship Blueprints"
---
## Structure Elements (SSC): 

Structural element count `= volume / 21`, rounded down. This is the number of `SSC` materials needed for the ship.

## Volume of Components:

The volume of each component seems to be consistent regardless of the other blueprint choices on the ship:

Cargo bay volume. Upon investigation, the volume of each cargo bay is the capacity in m3 * 1.05, rounded up.

| Cargo Bay | Volume (m3) |
| ----------- | ----------- |
| T | 105 |
| VS | 263 |
| S | 525 |
| M | 1050 |
| L | 2100 |
| HL | 1050 |
| HV | 3150 |

FTL Tank Volume can be determined exactly, because it's possible to build an STL ship without an FTL tank. (assuming no tank contributes no volume)

| FTL Tank | Volume (m3) |
| ----------- | ----------- |
| None | 0 |
| Small | 3 |
| Medium | 9 |
| Large | 21 |

STL Tank relative volume can be determined exactly, but since we can't build a ship without any tank, the absolute size of tanks is unclear. Starting with an STL Small tank of 70m3 produces some nice round numbers.

| STL Tank | Volume (m3) |
| ----------- | ----------- |
| Small | 70 |
| Medium | 196 |
| Large | 480 |

FTL Engine Volume can be determined exactly, relative to the no-drive scenario:

| FTL Engine | Volume (m3) |
| ----------- | ----------- |
| None | 0 |
| Standard | 126 |
| Quickcharge | 133 |
| High-power | 243 |
| Hyper-power | 253 |

The STL Drive can't be removed, so consider these to be relative volumes. In this case they are optimized to account for all remaining volume on the ship, since this is the last volume-affecting component. The made up volume could be divided between the small STL tank and the Standard engine, however. 

| STL Engine | Volume (m3) |
| ----------- | ----------- |
| Standard | 239 |
| Fuel Saving | 238 |
| Glass | 238 |
| Advanced | 452 |
| High-power | 456 |

## Mass of Components
Mass of components is not consistent- it changes based on your other design choices. 

How much mass is added for each cargo bay, depending on your fuel tank size?
| Cargo Bay | With two small tanks (m3) | With two Med tanks |
| ----------- | ----------- |----------- |
| T | 0 | 0 |
| VS | 83 | 72 |
| S | 213 | 203 |
| M | 448 | 428 |
| L | 883 | 873 |
| HL | 548 | ? |
| HV | 1199 | 1179 |

Other oddities regarding mass of tanks vs. cargo size:

|tank size |Tiny|	Very Small|	Small cargo|	Med Cargo|	Large Cargo|	HV|	HL|
| ----------- | ----------- |----------- |----------- |----------- |----------- |----------- |----------- |
|STL Fuel Med|	76|	76|	76|	66|	66|	56|	|
|FTL Fuel Med|	16|	15|	16|	16|	26|	16|	|
|STL Fuel Large|	410	| 409 | 245 | 260 | 251 | 205 | 251?  |			
|FTL Fuel Large|	52	| 52 | 52 | 52 | 62 | 52 | 52? |

Note that as the cargo bay gets larger, the tanks sometimes add more mass, and sometimes add less mass.

## Component sizes:
Do the sizes of indivual ship components adjust the mass and volume of the ship by the same amoutn when installed? Evidence points to no, component mass/vol is irrelevant to ship size.			

## Build time:
`Build time = Operating Empty Mass / 50`

## Emitter count:

The source code lists some numbers, but there's no obvious way to apply them correctly.
|   | FTL_FIELD_EMITTER_SMALL | FTL_FIELD_EMITTER_MED | FTL_FIELD_EMITTER_LARGE |
| ----------- | ----------- |----------- |----------- |
| FTL_VOLUME_SPAN | 250 | 500 | 1000 |
| POWER_REQUIREMENT | 100 | 175 | 300 |

The dev blog suggests:
>For a ship to be able to perform FTL jumps in our PrUniverse, it has to surround itself with an FTL field. This field is created from a number of emitters placed on the ship. Each emitter can span a certain volume. Now, for the first iteration of ship building, ships will just have to have a set number of emitters that together span at least the ship's total volume.
 (source https://prosperousuniverse.com/blog/2020/07/20/warp-drive-time-244/ )

But the emitter requirement doesn't simply add up to the Span numbers. They also don't line up if you assume they are radii or diameters of spheres. 

